## To Do

- [ ] Add different sorting method
- [ ] Add markdown formatting to the comment
- [ ] Refactor PlaylistAddPage and FormAlbums
- [ ] Add sharing functionality (to other social network, not only Mastodon)
- [ ] Add possibility to create a meta-post with for example a weekly playlist
- [ ] Add configuration for invidious instance (invidious.fdn.fr is hardcoded for now)
- [ ] Change Privacy Policy about third-party embed