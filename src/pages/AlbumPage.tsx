import React, {useEffect, useState} from "react"
import {useNavigate, useParams} from "react-router-dom";
import AlbumEdition from "../components/forms/AlbumEdition";
import {toast} from "react-toastify";
import ConfirmDialog from "../components/ConfirmDialog";
import { IAlbum } from "../@types/album";
import { apiDeleteAlbum, apiGetOneAlbumById } from "../services/AlbumService";
import AlbumItem from "../components/items/AlbumItem";

type Props = {
    token: string
}

export default function AlbumPage({token}: Props) {
    const navigate = useNavigate();
    const {id} = useParams()
    const [album, setAlbum] = useState<IAlbum | null>(null)
    const [isEditing, setIsEditing] = useState(false)
    const [refresh, setRefresh] = useState(false)
    const [confirm, setConfirm] = useState(false)

    function handleEditClick() {
        setIsEditing(true)
    }

    function handleDeleteClick() {
        setConfirm(true)
    }

    function deleteAlbum() {
        if (id !== null) {
            return;
        }
        return apiDeleteAlbum(parseInt(id), token).then(response => {
            if (response.ok) {
                toast("Album deleted successfully")
                navigate("/")
            } else {
                console.log(response)
            }
        });
    }

    useEffect(() => {
        async function fetchAlbum() {
            if (id !== null) {
                return;
            }
            setRefresh(false)
            const response = await apiGetOneAlbumById(parseInt(id), token);

            if (response === null) {
                navigate("/")
            } else {
                setAlbum(response)
            }
        }

        fetchAlbum();
    }, [id, token, refresh, navigate]);

    if (album) {
        document.title = `${album.artist} - ${album.title} | `+process.env.REACT_APP_TITLE!;

        return (
            <main>
                <ConfirmDialog
                    title={"Delete album?"}
                    content={"Do you really want to delete this album?"}
                    onConfirm={deleteAlbum}
                    openState={confirm}
                    setOpenState={setConfirm}
                    cancelButtonText="No"
                    confirmButtonText="Yes"
                    onCancel={() => {}}
                />
                <AlbumEdition
                    token={token}
                    setRefresh={setRefresh}
                    isEditing={isEditing}
                    setIsEditing={setIsEditing}
                    isAdding={false}
                    setIsAdding={() => {}}
                    albumInEdition={album}
                    setAlbumInEdition={() => {}}
                    addingPlaylistItem={false}
                    callback={()=>{}}
                />
                <div>
                    <AlbumItem
                        key={album.id}
                        token={token}
                        editButtonCallback={handleEditClick}
                        deleteButtonCallback={handleDeleteClick}
                        addToPlaylistCallback={null}
                        onItemOpenCallback={null}
                        album={album}
                    />
                    <p className="text-center"><a href="/">Return to home</a></p>
                </div>
            </main>
        )
    }

    return ""
}