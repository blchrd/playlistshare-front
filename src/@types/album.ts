import { IPaginationDataLinks, IPaginationDataMeta } from "./paginationData"

export interface IAlbum {
    id: number,
    artist: string,
    title: string,
    genre: string[],
    url: string,
    bandcampItemId: string | null,
    bandcampItemType: string | null,
    average: number,
    inConnectedUserPlaylist: boolean,
}

export interface IAlbumPayload {
    artist: string,
    title: string,
    genre: string[],
    url: string,
}

export interface IAlbumPaginatedCollection {
    albums: IAlbum[],
    links: IPaginationDataLinks | [],
    meta: IPaginationDataMeta | []
}