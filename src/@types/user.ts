import { Serializable } from "child_process";

export interface IUser {
    id: number,
    name: string
}

export interface IUserProfile {
    id: number,
    name: string,
    email: string,
    token: string,
    is_admin: boolean,
}

export interface IUserAdmin {
    id: number,
    name: string,
    email: string,
    is_admin: string,
    email_verified_at: Date | null,
}

export interface IUserAdminPayload {
    id: number,
    name: string,
    email: string,
    verified: boolean,
}

export type UserContextType = {
    userProfile: IUserProfile | null;
    updateUserProfile: (profile: IUserProfile) => void;
    removeUserProfile: () => void;
};