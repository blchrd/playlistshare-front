# Playlist Share Frontend

This repository contains the ReactJS frontend application for my Playlist Share project.

![Screenshot of Playlist Share](screenshot.png)

This application allows user (singular, but multi-user is on the roadmap) to share albums they have listened to, along with notation and comment. This project aims to provide a way to share their listening, and got them all over one single place.

This project is still in development, you can use it in production if you wish, but note that this comes with no guaranty.

## Features

* **User authentication**: main user is set up directly in the backend for now.
* **User registration**: User registration is possible but I recommend to disable it in the admin interface for now. You can manually validate user in the admin interface if you don't have working SMTP server for the validation mail.
* **Album submission**: authenticated user can submit albums they have listened to along with ratings and comments
* **Album listing**: display a list of shared albums with their notations and comments
* **Private album**: user can decide to put an album in private, only the user can see it
* **Search system**: user can use advanced search feature to filters the albums
* **Adding existing album in the playlist**: add album from the search of the home page in your playlist.
* **User profile**: For now it is just the username, but I plan to add more in the future.
* More to come.

## Prerequisites

Before running the application, ensure you have the following prerequisites installed:

* Node.js
* npm (Node Package Manager)
* Playlist Share backend instance to connect with

## Getting started

Follow these instructions to set up and run the frontend application locally:

1. Clone the repository:

       git clone https://framagit.org/blchrd/playlistshare-front.git
       cd playlistshare-front

2. Install the dependencies:

       npm install

3. Configure environment variables: rename the `.env.example` file to `.env` and update the necessary environment variables (see [Configuration](#Configuration))

4. Start the development server:

       npm start

The application will be accessible at `http://localhost:3000` in your web browser.

## Configuration

* `REACT_APP_API_URL`: base url for backend API
* `REACT_APP_DEBUG`: enable or disable debug mode (which is not really implemented right now)
* `REACT_APP_TITLE`: Title of the application
* `REACT_APP_MAX_ITEM_PER_PAGE`: Number of item by page

By default, the terms of service and the privacy policy are empty, and will not be displayed.
To create and or update these documents, after installation, go to the administration in the appropriate section.

Templates for both document are located in the `template` folder of this repository.

## Contributing

Thank you for your interest in contributing to Playlist Share! At the moment, we are not accepting direct contributions to this repository. However, you are welcome to fork the repository and work on any enhancements or modifications independently.

[See all repositories for PlaylistShare](https://framagit.org/playlistshare/)

## License

This work is licensed under Mozilla Public License Version 2.0, see the [LICENSE](LICENSE) file for details.
